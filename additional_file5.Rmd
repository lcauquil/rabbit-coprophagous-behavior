---
title: "Additional file 6"
description: 
   List of the differentially expressed genes (|log2(fold change)|> 0.5 and p.adj value < 0.05) during the two weeks following weaning (D35 vs D49) in the ileum of rabbit that ingested feces from unrelated does treated with antibiotic  (**FFab** group :\ ingestion of Faeces from Foreign antibiotic treated females) in the nest during early life.
output: 
  distill::distill_article
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r}
pacman::p_load(dplyr,
               tidyr,
               gt,
               flextable,
               downloadthis)
```

```{r}
load(file = "RData/data_DEG.RData")
```

```{r}
list_oligo$d35vs49_FFab %>% 
   dplyr::select(Associated.Transcript.Name, Ensembl.Gene.ID, ProbeName, Sequence, Human.Ensembl.Gene.ID, logFC, adj.P.Val) %>% 
  # rename("Gene Symbol" = "Associated.Transcript.Name",
  #        "Ensembl Gene ID" = "Ensembl.Gene.ID",
  #        "Probe Name" = "ProbeName",
  #        "Human Ensembl Gene ID" = Human.Ensembl.Gene.ID,
  #        "adj P-Val" = adj.P.Val) %>% 
  download_this(output_name = "list oligo d35vs49 FFab", 
                output_extension = ".csv",
                button_label = "Download data as csv",
                button_type = "default",
                has_icon = TRUE,
                icon = "fa fa-save")

```

```{r}
list_oligo$d35vs49_FFab %>% 
  dplyr::select(Associated.Transcript.Name, Ensembl.Gene.ID, logFC, adj.P.Val,ProbeName, Sequence, Human.Ensembl.Gene.ID ) %>% 
  
  flextable %>% 
  autofit %>% 
  fit_to_width(max_width = 20)
```


