## function to select DEG for a define contrast

oligo_filter <- function(data, filter, contrast, anno)
{
  ## filtre sur le contraste, la methode de correction, la p-value et le fold-change, reprend les rownames (oligos) en colonne[1]
  tmp <- topTable(data,
                  coef = contrast,
                  genelist = data$genes,
                  number = Inf,
                  adjust.method = "BH",
                  p.value = 0.05,
                  sort.by = "p",
                  lfc = 0.5) %>%
    rownames_to_column(var = "oligo")
  
  if (filter == "none")  ## DEG up and down
    tmp <- tmp
  if (filter == "up")
    tmp <- filter(tmp, tmp$logFC > 0)
  if (filter == "down")
    tmp <- filter(tmp, tmp$logFC < 0)
  
  
  ## select oligos  and several annotation columns
  tmp1 <- anno[anno$oligo %in% tmp$oligo,]
  ## merge annotations and statistics
  oligos <- inner_join(tmp[,c("oligo",
                              "logFC",
                              "P.Value",
                              "adj.P.Val")], tmp1)
  ## filter and sort by oligos
  if (filter == "none")
    oligos <- oligos %>% arrange(oligos$logFC)
  if (filter == "up")
    oligos <- oligos %>% arrange(desc(oligos$logFC))
  if (filter == "down")
    oligos <- oligos %>% arrange(oligos$logFC)
  
  return(oligos)
  rm(tmp, tmp1)
}

## function to plot DEG expression

plot_data_column = function (data, list) {
  data %>%
    filter(oligo == list) -> tmp
  y_bar <- sum(tmp[which.max(tmp$mean), c("mean", "sd")]) + .5
  
  signif <- list_oligo$d35vs49_FF_UP$adj.P.Val[list_oligo$d35vs49_FF_UP$oligo == list]
  
  data %>%
    filter(oligo == list) %>%
    ggplot(aes(x = factor(treatment, level = correct_treat),
               y = mean,
               group = factor(age, level = correct_age))) +
    
    geom_bar(position = position_dodge(),
             stat = "identity",
             fill = correct_col2,
             col = "black") +
    
    geom_errorbar(aes(ymin = mean - sd,
                      ymax = mean + sd),
                  width = .2,
                  position = position_dodge(.9)) +
    
    annotate("segment", x = 0.8, xend = 1.2, y = y_bar, yend = y_bar,  colour = "black", size = 1) +
    annotate("text", x = 1, y = y_bar+ .1,  colour = "black", 
             label = ifelse(signif < 0.001, "***", ifelse(signif < 0.01, "**", "*")),
             size = 8) +
    
    
    labs(title = paste0("Expression of gene ", 
                        anno_merge_DEG$Associated.Transcript.Name[anno_merge_DEG$oligo == list])) +
    labs(x = "Treatment per day", y = "Expression") +
    theme_classic() -> pp
  
  A1 <- rectGrob(height = .5, width = .5, gp = gpar(fill = "#82ccdd", col = "black"))
  A2 <- rectGrob(height = .5, width = .5, gp = gpar(fill = "#6a89cc", col = "black"))
  A3 <- rectGrob(height = .5, width = .5, gp = gpar(fill = "#a4b0be", col = "black"))
  A4 <- rectGrob(height = .5, width = .5, gp = gpar(fill = "#747d8c", col = "black"))
  A5 <- rectGrob(height = .5, width = .5, gp = gpar(fill = "#b8e994", col = "black"))
  A6 <- rectGrob(height = .5, width = .5, gp = gpar(fill = "#78e08f", col = "black"))
  
  T1 <- textGrob("FF_d35", x = .05, just = "left")
  T2 <- textGrob("FF_d49", x = .05, just = "left")
  T3 <- textGrob("FFab_d35", x = .05, just = "left")
  T4 <- textGrob("FFab_d49", x = .05, just = "left")
  T5 <- textGrob("NF_d35", x = .05, just = "left")
  T6 <- textGrob("NF_d49", x = .05, just = "left")
  
  # Construct a gtable - 2 columns X 7 rows
  leg = gtable(width = unit(c(1,3), "cm"), height = unit(c(1,1,1,1,1,1,1), "cm"))
  leg = gtable_add_grob(leg, rectGrob(gp = gpar(fill = NA, col = "black")), t=2,l=1,b=7,r=2)
  
  # Place the six grob into the table
  leg = gtable_add_grob(leg, A1, t=2, l=1)
  leg = gtable_add_grob(leg, A2, t=3, l=1)
  leg = gtable_add_grob(leg, A3, t=4, l=1)
  leg = gtable_add_grob(leg, A4, t=5, l=1)
  leg = gtable_add_grob(leg, A5, t=6, l=1)
  leg = gtable_add_grob(leg, A6, t=7, l=1)
  leg = gtable_add_grob(leg, T1, t=2, l=2)
  leg = gtable_add_grob(leg, T2, t=3, l=2)
  leg = gtable_add_grob(leg, T3, t=4, l=2)
  leg = gtable_add_grob(leg, T4, t=5, l=2)
  leg = gtable_add_grob(leg, T5, t=6, l=2)
  leg = gtable_add_grob(leg, T6, t=7, l=2)
  
  # Give it a title (if needed)
  leg = gtable_add_grob(leg, textGrob("Group"), t=1, l=1, r=2)
  
  # Get the ggplot grob for plot1
  g = ggplotGrob(pp)
  
  # Get the position of the panel,
  # add a column to the right of the panel, 
  # put the legend into that column, 
  # and then add another spacing column
  pos = g$layout[grepl("panel", g$layout$name), c('t', 'l')]
  g = gtable_add_cols(g, sum(leg$widths), pos$l)
  g = gtable_add_grob(g, leg, t = pos$t, l = pos$l + 1)
  g = gtable_add_cols(g, unit(6, "pt"), pos$l)
  
  # Draw it
  #grid.newpage()
  grid.draw(g)
  
}


