---
title: "Ileon microbiota"
description:
  Analysis of the ileon microbiota
output: 
  distill::distill_article:
    toc: true
    toc_float: true
    css: "styles.css"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r, warning=FALSE, message=FALSE}
library(phyloseq)
library(tidyverse)
library(DT)
#library(vegan)
library(ape)
library(stringr) # pour scinder une variable
library(tibble)
library(ggplot2)
library(RColorBrewer)
library(ggsci) #pour générer des palettes
library(dplyr)
library(plotly)
library(ggrepel)
library(wesanderson) #PALETTE
library(flextable)



#manipulation resultat stat
library(broom)
library(broom.mixed)
library(purrr) #map_dfr
library(car)


```


# import data

```{r, warning=FALSE}

load("RData/Galaxy27-[FROGSSTAT_Phyloseq_Import_Data__data.Rdata].rdata")
frogs.data<-data

# removed Cluster_1548  75 % percentage coverage

badTaxa = c("Cluster_1548")
allTaxa = taxa_names(frogs.data)
myTaxa <- allTaxa[!(allTaxa %in% badTaxa)]
frogs.data = prune_taxa(myTaxa, frogs.data)

#translate metadata
fact<-data.frame(sample_data(frogs.data)) |>
        mutate(treatment = factor(case_when(lot == "CAB"  ~ "FFab",
                                  lot == "CC"  ~ "FF",
                                  lot == "PP"  ~ "NF"))) |> 
  mutate(age = factor(case_when(age == "35"  ~ "d35",
                                  age == "49"  ~ "d49"))) |> 
  mutate(contrast = factor(paste(age, treatment, sep = "_"))) |> 
  mutate(id = abat  )|>
  select(SampleID, id, treatment, age, contrast)

sample_data(frogs.data) <-(fact)

CONTRAST<-  c("#A6CEE3","#FB9A99","#B2DF8A", "#1F78B4","#E31A1C", "#33A02C") 
  
                                

```


# sequencing report

```{r}
Sample<-length(sample_data(frogs.data)$SampleID)
Mean<-mean(sample_sums(frogs.data))
sd<-sd(sample_sums(frogs.data))
min<-min(sample_sums(frogs.data))
max<-max(sample_sums(frogs.data))
total<-sum((sample_sums(frogs.data)))
tab1<-as.data.frame(rbind(Sample,total,Mean,sd,min,max))
tab1<- tibble::rownames_to_column(tab1, "Parameters")
tab1[,2] <- round(tab1[,2], 0)
colnames(tab1)<-c("Parameters","Values")
ft<-flextable::flextable(tab1)
colformat_num(ft, big.mark=",")
```


```{r rarefying count using phyloseq}
frogs.data_raref <- rarefy_even_depth(frogs.data, rngseed = 24)

```

# beta diversity


```{r, fig.height=8, fig.width=8}

capture.output(ord <- ordinate(frogs.data_raref, method = "MDS", distance = "bray"), file='NULL')


#mini<-min(ord$points)-0.25
#maxi<-max(ord$points)+0.25
p.o <- plot_ordination(frogs.data_raref, ord, color = "contrast") + 
          geom_point(size = 3) + 
          scale_color_manual(values=CONTRAST)+ labs(color = "Groups")
 

p.o <- p.o + theme_classic() + ggtitle("PCoA + Bray-Curtis-Index") ## add title and plain background
p.o <- p.o + coord_fixed(ratio = 1)+  #, xlim = c(mini, maxi), ylim = c(mini, maxi))+
  
  
  theme(axis.line=element_line(size = 0.8, color = "black"), 
        axis.ticks.length = unit(.2, "cm"), 
        axis.text = element_text(size = 12, color = "black"), 
        axis.title = element_text(size = 12, color = "black"), 
        legend.text = element_text(size = 12), 
        legend.title = element_text(size = 12), legend.key.height = unit(.7, "cm"))
 #geom_text_repel(aes(label=double), size = 3)
#coord_cartesian(xlim = c(mini, maxi), ylim = c(mini, maxi)) +

p.o
```





# stat function

```{r}
## le modèle
fit_model <- function(data) {
    mod<-stats::lm(Abundance ~ treatment * age, data = as.data.frame(data))
   }
## fonction récupération pval
p_val <- function(data) {
  car::Anova(data, type = "III")
}



```


# alpha diversity

```{r}
alpha.diversity_raref <- estimate_richness(frogs.data_raref,
           measures = c("Observed", "Shannon","InvSimpson"))
```

```{r, fig.height=6, fig.width=8}
# plot richness_rarefied
p <- plot_richness(frogs.data_raref, x="contrast",
                   measures = c("Observed", "Shannon", "InvSimpson"))
p <- p + geom_boxplot(aes(fill = contrast)) +
        scale_fill_manual(values = CONTRAST) +
        theme_bw() + 
        labs( fill="Groups",  x = NULL) +
        theme(axis.text.x = element_text(colour="black", size=10, angle=45, hjust=1, vjust=1, face="bold"),
              axis.text.y = element_text(colour="black", size=10, angle=0, hjust=0.5, vjust=0, face="bold"),
              axis.title.x = element_text(colour="black", size=12, angle=0, hjust=0.5, vjust=0, face="bold"),
              axis.title.y = element_text(colour="black", size=12, angle=90, hjust=.5, vjust=2.5, face="bold"),
              legend.text = element_text(size = 10),
              legend.title = element_text(size = 12, face="bold"))

#to plot points
plot(p)
```
Preparing data  

```{r}

div <- cbind(sample_data(frogs.data_raref), alpha.diversity_raref)
#paste(sample_data(frogs.data_raref)$SampleID, rownames(alpha.diversity_raref),sep="=")

data_long <-div |> 
    pivot_longer(cols = Observed:InvSimpson, 
               names_to = "Parameter",
               values_to = "Abundance")
```


```{r}
options(contrasts = c("contr.sum", "contr.poly"))
```

```{r}
result_pval<-data_long %>%
  split(data_long$Parameter)%>%
  map(fit_model) %>%
  map(p_val) %>%
  map_dfr(tidy, .id = "indice") %>% 


  dplyr::select(indice, term, p.value) %>% 
  pivot_wider(names_from = term,
              values_from = p.value)
result_pval[,2:5]<- round(result_pval[,2:5],4)

datatable(result_pval[,1:5])
```

# Taxonomy  
```{r}
#cleaning taxonomy

frogs.data.taxcorr<-frogs.data
#vecteur de cluster pour correction unknown genus si< 95%
load(file="RData/SC1001_ileon_taxcorr.Rdata")

# correction genus <95
#UnK_genus_cluster<-readClipboard() #importation à partir du fichier excel
tax_table(frogs.data.taxcorr)[UnK_genus_cluster, "Genus"]<-"unknown genus"


#Prevotella 7;9
#Prevotella79_genus_cluster<-readClipboard()
tax_table(frogs.data.taxcorr)[Prevotella79_genus_cluster, "Genus"]<-"Prevotella"

#multi-affiliation resolution
tax_table(frogs.data.taxcorr)["Cluster_17", "Genus"]<-"Variovorax"
tax_table(frogs.data.taxcorr)["Cluster_83", "Genus"]<-"Roseburia"
tax_table(frogs.data.taxcorr)["Cluster_89", "Genus"]<-"Romboutsia"

tax_table(frogs.data.taxcorr)[c("Cluster_421", "Cluster_518"), "Genus"]<-"unknown genus"



#save(UnK_genus_cluster, Prevotella79_genus_cluster, file="../data/SC1001_ileon_taxcorr.Rdata")

```

```{r}


tab_Phylum<-tax_glom(frogs.data.taxcorr, "Phylum")
#tax_table(tab_Phylum)
#ntaxa(tab_Phylum)
tab_Family<-tax_glom(frogs.data.taxcorr, "Family") #objet phyloseq
#as.data.frame(tax_table(tab_Family))%>%dplyr::arrange(Family) |> select(Family) |> unique()
#ntaxa(tab_Family)
tab_Genus<-tax_glom(frogs.data.taxcorr, "Genus") #objet phyloseq
#as.data.frame(tax_table(tab_Genus))%>%dplyr::arrange(Genus)|> select(Genus) |> unique()



```



## Phylum

```{r}
## Transforme in relative abundance 
relatabunphy <- transform_sample_counts(tab_Phylum, function(OTU) OTU/sum(OTU))
```
Table OTU au format long  
```{r}
dfrap <- psmelt(relatabunphy) |>   #fichier format long
   dplyr::select(Phylum, Abundance, id:contrast) %>%
   pivot_wider(names_from = Phylum,
              values_from = Abundance) %>% 
  mutate(Firm_bact_ratio = Firmicutes/Bacteroidota) %>%
  pivot_longer(cols = where(is.numeric),
               names_to = "Phylum", 
               values_to = "Abundance") 
  
```

### ANOVA 

```{r}
dfrap$Abundance<-dfrap$Abundance^0.25
result_pval<-dfrap %>%
  split(dfrap$Phylum)%>%
  map(fit_model) %>%
  map(p_val) %>%
  map_dfr(tidy, .id = "Phylum") %>% 

#result_pvaltot<-result_pval 
 
  dplyr::select(Phylum, term, p.value) %>% 
  pivot_wider(names_from = term,
              values_from = p.value)
result_pval[,2:5]<- round(result_pval[,2:5],4)

datatable(result_pval[,c(1,3:5)])

```
### mean and sem  

```{r}
### calcul des moyennes sd et SEM

dfrap <- psmelt(relatabunphy) |>   #fichier format long
   dplyr::select(Phylum, Abundance, id:contrast) %>%
   pivot_wider(names_from = Phylum,
              values_from = Abundance) %>% 
  mutate(Firm_bact_ratio = Firmicutes/Bacteroidota) %>%
  pivot_longer(cols = where(is.numeric),
               names_to = "Phylum", 
               values_to = "Abundance") 

m_dfrap<-dfrap%>%
         dplyr::select(.,Phylum, contrast,id, Abundance)%>%
         dplyr::filter(!Phylum == "Firm_bact_ratio") |> 
         group_by(contrast,Phylum)%>%
         dplyr::summarise(moyAbund=mean(Abundance)*100,sem = sd(Abundance) / sqrt(length(Abundance))*100)

#   firmicutes/bacteroidota ratio
tt<-dfrap%>%
  dplyr::select(.,Phylum, contrast, Abundance)%>%
  dplyr::filter(Phylum == "Firm_bact_ratio") |> 
  group_by(contrast)%>%
  dplyr::summarise(moyAbund=mean(Abundance),
                   sem = sd(Abundance) / sqrt(length(Abundance))) |>
  mutate(Phylum = "Firm_bact_ratio") |> 
  relocate(Phylum, .before = contrast)
  

temp <-rbind(m_dfrap,tt)
temp[,3:4]<-round(temp[,3:4],3)
datatable(temp)

temp2<-temp |>  pivot_wider(names_from = contrast, values_from = c(moyAbund, sem))

final<-left_join(temp2, result_pval)
xlsx::write.xlsx(x = as.data.frame(final), file = "data/phylum_Tables.xlsx",
                  sheetName = "Phylum",
                  append = FALSE, row.names = FALSE)

rm(tmp, df_stat)
```

### graph
```{r, fig.height=7}

pphyl<-ggplot(data=m_dfrap, 
       aes(x=contrast, y=(moyAbund), fill=Phylum)) +
       geom_bar(stat="identity", position = "stack") +
  #facet_wrap(~sexe_temps)+
  
  scale_fill_brewer(palette = "Spectral") +
theme_classic() + 
theme(text = element_text(size = 20), legend.key.size = unit(1.5, "lines"), legend.text = element_text(size = 12), legend.title = element_text(size = 15), legend.position = "right", panel.background = element_rect(fill = "white", colour = "white"), axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 15), axis.title = element_text(size = 15), axis.text.x = element_text(angle = 90, hjust = 0), legend.key = element_rect(size = 2, color = 'white')) +
labs(x = "Groups") + labs(y= "Proportion")  

#newSTorder=c("CTL","WTD")
#pphyl$data$lot <- factor(pphyl$data$lot, levels=newSTorder)
pphyl
```
### barplot

```{r}
m_dfrap |> 
  filter (Phylum == "Firmicutes") |> 
    ggplot() +
   geom_bar(aes(x=contrast, y=moyAbund, fill=contrast),
            stat="identity", 
            position=position_dodge()) +
   geom_errorbar(aes(x=contrast,ymin=moyAbund-sem, ymax=moyAbund+sem), width=.2,
                 position=position_dodge(.9)) +
                 
   scale_fill_manual(name="Groups",values=CONTRAST) + 
  theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 0, hjust = 0.5), legend.key = element_rect(size = 2, color = 'white'))+
  labs(x = "",y= "Proportion", title="Firmicutes")



```

```{r}
m_dfrap |> 
  filter (Phylum == "Proteobacteria") |> 
    ggplot() +
   geom_bar(aes(x=contrast, y=moyAbund, fill=contrast),
            stat="identity", 
            position=position_dodge()) +
   geom_errorbar(aes(x=contrast,ymin=moyAbund-sem, ymax=moyAbund+sem), width=.2,
                 position=position_dodge(.9)) +
                 
   scale_fill_manual(name="Groups",values=CONTRAST) + 
  theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 0, hjust = 0.5), legend.key = element_rect(size = 2, color = 'white'))+
  labs(x = "",y= "Proportion", title="Proteobacteria")
```


```{r}
m_dfrap |> 
  filter (Phylum == "Bacteroidota") |> 
    ggplot() +
   geom_bar(aes(x=contrast, y=moyAbund, fill=contrast),
            stat="identity", 
            position=position_dodge()) +
   geom_errorbar(aes(x=contrast,ymin=moyAbund-sem, ymax=moyAbund+sem), width=.2,
                 position=position_dodge(.9)) +
                 
   scale_fill_manual(name="Groups",values=CONTRAST) + 
  theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 0, hjust = 0.5), legend.key = element_rect(size = 2, color = 'white'))+
  labs(x = "",y= "Proportion", title="Bacteroidota")

```


```{r}
temp |> 
  filter (Phylum == "Firm_bact_ratio") |> 
    ggplot() +
   geom_bar(aes(x=contrast, y=moyAbund, fill=contrast),
            stat="identity", 
            position=position_dodge()) +
   geom_errorbar(aes(x=contrast,ymin=moyAbund-sem, ymax=moyAbund+sem), width=.2,
                 position=position_dodge(.9)) +
                 
   scale_fill_manual(name="Groups",values=CONTRAST) + 
  theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 0, hjust = 0.5), legend.key = element_rect(size = 2, color = 'white'))+
  labs(x = "",y= "Proportion", title="Firmicutes to Bacteroidota ratio")

```

## Family

```{r}
## Transforme en abondance relative
relatabunfam <- transform_sample_counts(tab_Family, function(OTU) OTU/sum(OTU))
```




Table OTU au format long  
```{r}
dfraf <- psmelt(relatabunfam) #fichier format long


```



```{r}
#recherche du nb de famille unknown et multi-affil
ttUK <- dfraf |> 
  filter(Family %in% c("unknown family")) |> 
  dplyr::select(OTU) |> 
  unique() |> tally()
ttMA <- dfraf |> 
  filter(Family %in% c("Multi-affiliation"))|> 
  dplyr::select(OTU) |> 
  unique()|> tally()

#Taux d'affiliation à la famille
#sequence UK_MA /seq tot en %

tt1<- dfraf |> 
  filter(Family %in% c("unknown family","Multi-affiliation")) 

tt2 <- 100-sum(tt1$Abundance)/sum(dfraf$Abundance)*100


```


there is `r ntaxa(tab_Family)` families whom `r ttMA` families with a multi-affiliation and `r ttUK` classify “unknown family”. Rate of affiliation is `r round(tt2,1)` %.

### Top family

```{r}

top_Family <- dfraf %>%
  dplyr::select(Family, Abundance, contrast) %>% 
  filter(!Family %in% c("unknown family","Multi-affiliation")) |> 
  group_by(Family,contrast) %>% 
  summarise_all(list(mean = mean)) %>%  
  dplyr::arrange(desc(mean)) %>% 
  dplyr::filter(mean >= 0.00499) %>%  
  dplyr::arrange(mean) %>%  
  distinct(Family) %>% 
  as_vector()
top_Family



```

### ANOVA 

```{r}

result_pval<- dfraf %>%
  split(dfraf$Family)%>%
  map(fit_model) %>%
  map(p_val) %>%
  map_dfr(tidy, .id = "Family") %>% 


  dplyr::select(Family, term, p.value) %>% 
  pivot_wider(names_from = term,
              values_from = p.value)
result_pval[,2:5]<- round(result_pval[,2:5],4)

result_pval<- result_pval |> 
  dplyr::select(-`(Intercept)`, -Residuals) |> 
  filter(Family %in% top_Family)
  
datatable(result_pval)
xlsx::write.xlsx(x = as.data.frame(result_pval), file = "data/family_Tables.xlsx",
                  sheetName = "family",
                   row.names = FALSE)


```



### mean  sem  

```{r}
### calcul des moyennes sd et SEM
m_dfraf<-dfraf%>%
         dplyr::select(.,Family, contrast, Abundance)%>%
         dplyr::filter(!Family == "unknown family") |> 
         group_by(Family,contrast)%>%
         dplyr::summarise(moyAbund=mean(Abundance)*100,sem = sd(Abundance) / sqrt(length(Abundance))*100)


temp <- m_dfraf

temp[,3:4]<-round(temp[,3:4],3)
datatable(temp)
```
### bubbleplot

```{r}

p<-m_dfraf |> 
  filter(Family %in% top_Family[10:30]) |> 
  ggplot( aes(x=contrast , y=reorder(Family,-moyAbund), size = (moyAbund),  color = contrast)) +
        geom_point(alpha=0.5) +
        scale_size(range = c(.1, 10))+
    scale_color_manual(values=CONTRAST)+ labs(color = "Groups", size = "Relative abundance")
 
p+ theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 0.8, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 45, hjust = 1), legend.key = element_rect(size = 2, color = 'white'))+
  #theme(plot.margin = margin(1,1,1.5,1.2, "cm"))+
  labs(x = "", y= "")
ggsave(file="family_plot.tiff")
```



### barplot
```{r}
m_dfraf |> 
  filter (Family == "Lactobacillaceae") |> 
    ggplot() +
   geom_bar(aes(x=contrast, y=moyAbund, fill=contrast),
            stat="identity", 
            position=position_dodge()) +
   geom_errorbar(aes(x=contrast,ymin=moyAbund-sem, ymax=moyAbund+sem), width=.2,
                 position=position_dodge(.9)) +
                 
   scale_fill_manual(name="Groups",values=CONTRAST) + 
  theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 0, hjust = 0.5), legend.key = element_rect(size = 2, color = 'white'))+
  labs(x = "",y= "Proportion", title="Lactobacillaceae")



```








## Genus

```{r}
## Transforme en abondance relative
relatabungen <- transform_sample_counts(tab_Genus, function(OTU) OTU/sum(OTU))
```
Table OTU au format long  
```{r}
dfrag <- psmelt(relatabungen) #fichier format long
```


```{r}
#recherche du nb de famille unknown et multi-affil
ttUK <- dfrag |> 
  filter(Genus %in% c("unknown genus")) |> 
  dplyr::select(OTU) |> 
  unique() |> tally()
ttMA <- dfrag |> 
  filter(Genus %in% c("Multi-affiliation"))|> 
  dplyr::select(OTU) |> 
  unique()|> tally()

#Taux d'affiliation à la famille
#sequence UK_MA /seq tot en %

tt1<- dfrag |> 
  filter(Genus %in% c("unknown genus","Multi-affiliation")) 

tt2 <- 100-sum(tt1$Abundance)/sum(dfrag$Abundance)*100


```


There are `r ntaxa(tab_Genus)` genus whom `r ttMA` genus with a multi-affiliation et `r ttUK` classify “unknown genus”. Affiliation rate at the genus level is  `r round(tt2,1)`%.

### Top genus  

```{r}

top_Genus <- dfrag %>%
  dplyr::select(Genus, Abundance, contrast) %>% 
  filter(!Genus %in% c("unknown genus","Multi-affiliation")) |> 
  group_by(Genus,contrast) %>% 
  summarise_all(list(mean = mean)) %>%  
  dplyr::arrange(desc(mean)) %>% 
  dplyr::filter(mean >= 0.00499) %>%  
  dplyr::arrange(mean) %>%  
  distinct(Genus) %>% 
  as_vector()
top_Genus



```


### ANOVA 
```{r}
dfrag$Abundance<-dfrag$Abundance^0.25
result_pval<-dfrag %>%
  split(dfrag$Genus)%>%
  map(fit_model) %>%
  map(p_val) %>%
  map_dfr(tidy, .id = "Genus") %>% 


  dplyr::select(Genus, term, p.value) %>% 
  pivot_wider(names_from = term,
              values_from = p.value)
result_pval[,2:5]<- round(result_pval[,2:5],4)


result_pval<- result_pval |> 
  dplyr::select(-`(Intercept)`, -Residuals) |> 
  filter(Genus %in% top_Genus)
  
datatable(result_pval)

xlsx::write.xlsx(x = as.data.frame(result_pval), file = "data/genus_Tables.xlsx",
                  sheetName = "genus",
                   row.names = FALSE)
```
### mean  sem  

```{r}
### calcul des moyennes sd et SEM
m_dfrag<-dfrag%>%
         dplyr::select(.,Genus, contrast, Abundance)%>%
         dplyr::filter(!Genus == "unknown genus") |> 
         group_by(Genus,contrast)%>%
         dplyr::summarise(moyAbund=mean(Abundance)*100,sem = sd(Abundance) / sqrt(length(Abundance))*100)


temp <- m_dfrag

temp[,3:4]<-round(temp[,3:4],3)
datatable(temp)


```

### bubbleplot

```{r}

p<-m_dfrag |> 
  filter(Genus %in% top_Genus[21:41]) |> 
  ggplot( aes(x=contrast , y=reorder(Genus,-moyAbund), size = (moyAbund),  color = contrast)) +
        geom_point(alpha=0.5) +
        scale_size(range = c(.1, 10))+
    scale_color_manual(values=CONTRAST)+ labs(color = "Groups", size = "Relative abundance")
 
p+ theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 0.8, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 45, hjust = 1), legend.key = element_rect(size = 2, color = 'white'))+
  #theme(plot.margin = margin(1,1,1.5,1.2, "cm"))+
  labs(x = "", y= "")
ggsave(file="genus _plot.tiff")
```


### barplot  

```{r}
m_dfrag |> 
  filter (Genus == "Lactobacillus") |> 
    ggplot() +
   geom_bar(aes(x=contrast, y=moyAbund, fill=contrast),
            stat="identity", 
            position=position_dodge()) +
   geom_errorbar(aes(x=contrast,ymin=moyAbund-sem, ymax=moyAbund+sem), width=.2,
                 position=position_dodge(.9)) +
                 
   scale_fill_manual(name="Groups",values=CONTRAST) + 
  theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 0, hjust = 0.5), legend.key = element_rect(size = 2, color = 'white'))+
  labs(x = "",y= "Proportion", title="Lactobacillus")



```


```{r}
m_dfrag |> 
  filter (Genus == "Limosilactobacillus") |> 
    ggplot() +
   geom_bar(aes(x=contrast, y=moyAbund, fill=contrast),
            stat="identity", 
            position=position_dodge()) +
   geom_errorbar(aes(x=contrast,ymin=moyAbund-sem, ymax=moyAbund+sem), width=.2,
                 position=position_dodge(.9)) +
                 
   scale_fill_manual(name="Groups",values=CONTRAST) + 
  theme_classic()+
  theme( 
        title =element_text(size=9),
        legend.key.size = unit(1, "lines"), 
        legend.text = element_text(size = 11), legend.title = element_text(size = 10), legend.position = "right", 
        panel.background = element_rect(fill = "white", colour = "white"), 
        axis.line = element_line(size = 1, colour = "black"), axis.text = element_text(colour = "black", size = 10), axis.title = element_text(size = 10), axis.text.x = element_text(angle = 0, hjust = 0.5), legend.key = element_rect(size = 2, color = 'white'))+
  labs(x = "",y= "Proportion", title="Limosilactobacillus")



```









# Export final Rdata
```{r}

save(UnK_genus_cluster, Prevotella79_genus_cluster,tab_Phylum, tab_Family, tab_Genus, frogs.data, frogs.data_raref, frogs.data.taxcorr, file="data/SC1001_ileon_final.Rdata")
```


